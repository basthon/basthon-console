# Basthon-Console

## Build

    npm install
    npm start

This will install dependencies then build the app and open it with your default web browser (a local web server is started).

## More info

See [Basthon-Kernel's Readme](https://forge.apps.education.fr/basthon/basthon-kernel/-/blob/master/README.md#basthon).
