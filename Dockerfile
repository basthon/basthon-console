FROM debian:bookworm-slim

RUN apt-get update
RUN apt-get -y --no-install-recommends install make git symlinks python3 python3-setuptools python3-pip python3-jinja2 nodejs npm tar zip openssh-client rsync
RUN rm -rf /var/lib/apt/lists/*
