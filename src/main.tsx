import { StrictMode } from "react";
import { createRoot } from "react-dom/client";

import { ConsoleApp } from "@basthon/console-react";
import * as styles from "./styles.module.scss";

const root = document.createElement("div");
document.body.appendChild(root);
document.body.classList.add(styles.body);

createRoot(root).render(
  <StrictMode>
    <ConsoleApp />
  </StrictMode>,
);
