import { Configuration } from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
//@ts-ignore
import CreateFileWebpack from "create-file-webpack";
import path from "path";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import CopyPlugin from "copy-webpack-plugin";
import { dependencies } from "../package.json";
import { version as kernelVersion } from "../node_modules/@basthon/kernel-base/package.json";
import "webpack-dev-server";

const rootPath = path.resolve(__dirname, "..");
const buildPath = path.join(rootPath, "build");
const assetsPath = path.join(buildPath, "assets");
const consoleReactVersion = dependencies["@basthon/console-react"];

// build sys_info variable
const sysInfo = {
  "kernel-version": kernelVersion,
  "console-react-version": consoleReactVersion,
  "build-date": new Date().toISOString(),
};

// build version file
const versionFile = new CreateFileWebpack({
  content: JSON.stringify(sysInfo, null, 2),
  fileName: "assets/version",
  path: buildPath,
});

// generate index.html from template src/templates/index.html
const html = new HtmlWebpackPlugin({
  hash: true,
  sys_info: JSON.stringify(sysInfo),
  template: "./src/templates/index.html",
  filename: `index.html`,
  publicPath: "",
});

// bundle css
const css = new MiniCssExtractPlugin({
  filename: "assets/[name].[contenthash].css",
});

// copy examples, htaccess and kernel files
const copies = new CopyPlugin({
  patterns: [
    {
      // htaccess
      from: "./src/.htaccess",
      to: buildPath,
    },
    {
      // python3 files
      from: "**/*",
      context: "./node_modules/@basthon/kernel-python3/lib/dist/",
      to: path.join(assetsPath, kernelVersion, "python3"),
      toType: "dir",
    },
    {
      // examples
      from: "examples/**/*",
      to: buildPath,
      toType: "dir",
    },
  ],
});

const config: Configuration = {
  entry: "./src/main.tsx",
  output: {
    filename: "assets/[name].[contenthash].js",
    chunkFilename: "assets/[name].[contenthash].js",
    assetModuleFilename: "assets/[hash][ext][query]",
    path: buildPath,
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.module.scss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
      {
        test: /\.wasm$/i,
        type: "asset/inline",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    fallback: {
      vm: require.resolve("vm-browserify"),
      // for ocaml bundle
      constants: require.resolve("constants-browserify"),
      tty: require.resolve("tty-browserify"),
      fs: false,
      child_process: false,
      // for sql bundle
      crypto: require.resolve("crypto-browserify"),
      path: require.resolve("path-browserify"),
      buffer: require.resolve("buffer"),
      stream: require.resolve("stream-browserify"),
    },
  },
  plugins: [html, css, versionFile, copies],
  devServer: {
    static: {
      directory: buildPath,
    },
    devMiddleware: {
      writeToDisk: true,
    },
    compress: true,
    port: 8888,
  },
};

export default config;
